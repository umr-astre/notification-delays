library(readxl)         # For data import from excel
library(survival)       # For the survreg function. Provides Kaplan-Meier and Weibull models.
library(nimble)         # For Bayesian inference via MCMC
library(coda)           # For MCMC diagnostics
library(here)           # Provides path to .git
library(dplyr)          # For piping etc
library(nimbleNoBounds) # Pacakge to improve MCMC efficiency for parameters close to & bounded at zero
# library(survminer)    # For ggplots for survival curves. Not really needed.
source(here("R/scripts_weibull/functions.R"))
options(width=2500)

## Flag to control whether original (non-public) or simulated (public) dataset is to be used
useOriginalData <- TRUE # FALSE
if (useOriginalData) {
  notifDataRaw <- read_excel(here("../notification-delays-data/data/data_model.xlsx"),na="NA")
} else {
  notifDataRaw <- read.csv(here("simData/simulatedData.csv"))
}
names(notifDataRaw)
summary(notifDataRaw)

## Construct list of variables to be extracted from the excel file
listVar <- c(
  ## RESPONSE VARIABLES
  "Submission date", "Notification_delay",
  ## EPIDEMIOLOGICAL VARIABLES
  ## Hosts (wild/domestic); Incidence; Type (Primary/Secondary); Poultry per capita; Local poultry density
  "Species 1", "Evolution_cas_4_weeks", "Type", "Poultry_per_capita", "Ducks_density", "Chicken_density",
  ## ANIMAL HEALTH NETWORK
  ## Accessibilty score; Veterinarians per capita; Administrative levels
  "Accessibility_score", "Total_vet_density", "Administrative_levels",  # OTHER VARAIBLES IN EXCEL: Lab_vets_density, Practitioners_vet_density, PublicHealth_vet_density, Academic_vets_density
  ##
  ## SOCIOECONOMIC
  ## GDP; GNI; Stability score; Corruption score; Internet usage; Phone usage
  "GDP", "GNI", "Stability_score", "Corruption_score", "Internet_usage", "Phone_usage",
  ##
  ## CULTURAL
  ## Weekday; Public holiday
  "Country/Territory", "Weekday", "Event_on_holiday")

## Subset columns
notifDataRaw <- notifDataRaw[,listVar]
summary(notifDataRaw)


###################
###################
## Cleaning data ##
###################
###################
notifData <- notifDataRaw %>%
  mutate(across(where(is.character),as.factor)) %>%
  rename(subDate=`Submission date`) %>%
  rename(notifDelay=`Notification_delay`) %>%
  mutate(confDate=subDate-(notifDelay*60*60*24)) %>%
  rename(wild=`Species 1`) %>%
  mutate_at("wild", function(x) x=="Wild") %>%
  rename(primary=Type) %>%
  mutate_at("primary", function(x) x=="Primary") %>%
  rename(poultryPerCapita=Poultry_per_capita) %>%
  rename(duckDensity=Ducks_density) %>%
  rename(chickenDensity=Chicken_density) %>%
  rename(accessibility=Accessibility_score) %>%
  rename(vetDensity=Total_vet_density) %>%
  rename(adminLevels=Administrative_levels) %>%
  rename(stability=Stability_score) %>%
  rename(corruption=Corruption_score) %>%
  rename(internetUsage=Internet_usage) %>%
  rename(phoneUsage=Phone_usage) %>%
  rename(country=`Country/Territory`) %>%
  rename(weekDay=Weekday) %>%
  rename(holiday=Event_on_holiday) %>%
  rename(incidence=Evolution_cas_4_weeks)

notifData$weekDay  <- factor(notifData$weekDay, levels=c("lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche"))
notifData$holiday  <- notifData$holiday == "Yes"
notifData$subDate  <- as.Date(notifData$subDate)
notifData$confDate <- as.Date(notifData$confDate)

summary(notifData)

## hist(notifData$Evolution_cas_4_weeks, main="Evolution_cas_4_weeks", xlab="Evolution_cas_4_weeks")
## dev.copy2pdf(file="Evolution_cas_4_weeks.pdf")


#################################
#################################
## Cleaning data: Handling NAs ##
#################################
#################################

## Replace NAs in United Kingdom adminLevels with 2 (country, county)
notifData[notifData$country=="United Kingdom",]$adminLevels <- 2

## Show subset of data with NAs in covariates
subNA    <- subset(notifData, (is.na(notifData) %>% apply(MARGIN=1, FUN="any")==T))
subIndex <- (is.na(subNA) %>% apply(MARGIN=2, FUN="any")==T)
subNA    <- tibble(country=subNA$country, subDate=subNA$subDate, subNA[,subIndex]) %>% arrange(country, subDate)
subNA %>% print(n=75)

if (FALSE) { ## TRUE
  ## Bulgaria
  notifData %>% filter(country=="Bulgaria")    %>% select(country, subDate, accessibility) %>% print(n="unlimited")
  ## Denmark
  notifData %>% filter(country=="Denmark")     %>% select(country, subDate, chickenDensity, accessibility, vetDensity) %>% print(n="unlimited")
  ## Finland
  notifData %>% filter(country=="Finland")     %>% select(country, subDate, duckDensity, chickenDensity, vetDensity, accessibility) %>% print(n="unlimited")
  ## Greece
  notifData %>% filter(country=="Greece")      %>% select(country, subDate, duckDensity, chickenDensity, vetDensity, accessibility) %>% print(n="unlimited")
  ## Luxembourg
  notifData %>% filter(country=="Luxembourg")  %>% select(country, subDate, duckDensity, chickenDensity, vetDensity, accessibility) %>% print(n="unlimited")
  ## Netherlands
  notifData %>% filter(country=="Netherlands") %>% select(country, subDate, duckDensity, chickenDensity, vetDensity, accessibility) %>% print(n="unlimited")
  ## Poland
  notifData %>% filter(country=="Poland")      %>% select(country, subDate, duckDensity, chickenDensity, vetDensity, accessibility) %>% print(n="unlimited")
  ## Romania
  notifData %>% filter(country=="Romania")     %>% select(country, subDate, duckDensity, chickenDensity, vetDensity, accessibility) %>% print(n="unlimited")
  ## Serbia
  notifData %>% filter(country=="Serbia")      %>% select(country, subDate, duckDensity, chickenDensity, vetDensity, accessibility) %>% print(n="unlimited")
  ## Sweden
  notifData %>% filter(country=="Sweden")      %>% select(country, subDate, duckDensity, chickenDensity, vetDensity, accessibility) %>% print(n="unlimited")
}


########################################
## Reconstruct listVar with new names ##
########################################
notifData <- notifData %>% select(confDate, subDate, notifDelay, wild, incidence, primary, poultryPerCapita, duckDensity, chickenDensity, accessibility, vetDensity, adminLevels, GDP, GNI, stability, corruption, internetUsage, phoneUsage, country, weekDay, holiday) #  dayZero
listVar   <- names(notifData)[ which(!is.element(names(notifData), c("subDate", "notifDelay", "confDate"))) ]


## !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
## !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
## Filter out any observations with observations with NAs in covariates
## !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
## !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
print("THIS STEP SHOULDN'T BE NEEDED IF INTERPOLATION OF DENSITIES IS IMPROVED")
print("THIS STEP SHOULDN'T BE NEEDED IF INTERPOLATION OF DENSITIES IS IMPROVED")
print("THIS STEP SHOULDN'T BE NEEDED IF INTERPOLATION OF DENSITIES IS IMPROVED")
if (any(is.na(notifData[,listVar]))) {
  notifData <- subset(notifData, (!is.na(notifData[,listVar]) %>% apply(MARGIN=1, FUN="any")==T))
}
dim(notifData) # 1388 rows -> 1334 rows (more now UK admin levels is corrected)

## !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
## List countries remaining in analysis, and reset factor levels
## !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
print("THIS STEP SHOULDN'T BE NEEDED IF INTERPOLATION OF DENSITIES IS IMPROVED")
print("THIS STEP SHOULDN'T BE NEEDED IF INTERPOLATION OF DENSITIES IS IMPROVED")
print("THIS STEP SHOULDN'T BE NEEDED IF INTERPOLATION OF DENSITIES IS IMPROVED")
notifData$country = as.factor(as.character(notifData$country))
table(notifData$country)
countriesList <- levels(notifData$country)
nCountries    <- length(countriesList)


#########################
#########################
## Kaplan-Meier curves ##
#########################
#########################

# Kaplan-Meier with interval censored data
PDF <- FALSE # TRUE
if (PDF) pdf(file=here::here("figures/kaplanMeier_global.pdf"))
par(mfrow=c(1,1), mar=c(5,5,1,1))
y = with(notifData, Surv(time=notifDelay, time2=notifDelay+1, type="interval2"))
kaplanMeier = survfit(y~1)
plot(kaplanMeier) ## Identical to plot(y)
if (PDF) dev.off()


PDF <- FALSE # TRUE
if (PDF) pdf(file=here::here("../notification-delays-data/figures/kaplanMeier_country.pdf"))
par(mfrow=c(5,5), mar=c(1,1,1,1))
xMax = 25
kmCountry <- vector("list", nCountries)
for (ii in 1:nCountries) {
  kmCountry[[ii]] <- survfit(Surv(time=notifDelay, time2=notifDelay+1, type="interval2") ~ 1,
                             data = notifData %>% filter(country==countriesList[[ii]]))
  plot(kmCountry[[ii]])# , xlim=c(0, xMax))
  legend("topright", legend=countriesList[[ii]], bty="n")
}
if (PDF) dev.off()


##########################
##########################
## Simple Weibull model ##
##########################
##########################
PDF = FALSE # TRUE
w1  = survreg(Surv(time=notifDelay+1e-11, time2=(notifDelay+1), type="interval2")~1, data=notifData, dist="weibull")
LWD = 2
if (PDF) pdf(file=here::here("figures/survreg_global.pdf"))
par(mfrow=c(1,1), mar=c(5,5,1,1))
plot(kaplanMeier, lwd=LWD)
curve(1-pweibull(x, shape=1/w1$scale, scale=exp(w1$coefficients["(Intercept)"])),
      0, 60, n=1100, add=TRUE, col="red", lwd=LWD)
legend("topright", col=c("black", "red", "red"), lwd=LWD, cex=0.8,
       legend=c("Kaplan-Meier", "Weibull (max likelihood)"))
if (PDF) dev.off()


######################################################################
######################################################################
## Construct allDates, holidays and allDays for time-varying hazard ##
######################################################################
######################################################################
runSlow = any(!exists("allDates"), !exists("allDays"), !exists("holidays")) # TRUE # FALSE
if (runSlow) {
  print("Sourcing workingdays.R")
  source(here::here("R/scripts_covariates/workingdays.R"))
}

countryCodes
countryNames
cbind(countryCodes, countryNames)

notifData$countryCode = countryCodes[sapply(notifData$country, function(x) which(countryNames==x))] %>% as.factor()


## Visual check that there is a 1: correspondance
table(as.character(notifData$country), notifData$countryCode)
if(any(colSums(table(as.character(notifData$country), notifData$countryCode) > 0)!=1)) stop("Check notifData$countryCode")
if(any(rowSums(table(as.character(notifData$country), notifData$countryCode) > 0)!=1)) stop("Check notifData$countryCode")
## Check countryCode levels are alphabetical
if(!all(levels(notifData$countryCode) == sort(levels(notifData$countryCode)))) stop("Ensure notifData$countryCode levels are alphabetical")

allDates
allDays
holidays <- tibble(holidays)

min(notifData[["confDate"]])
max(notifData[["subDate"]])

########################################################
## Construct model matrix and time-varying equivalent ##
########################################################
X <- with(notifData, model.matrix( ~ -1 +
                                     ## Cultural
                                     countryCode +
                                     ## Epidemiological
                                     wild + incidence + primary + poultryPerCapita + duckDensity + chickenDensity +
                                     ## Animal Health Network
                                     accessibility + vetDensity + adminLevels +
                                     ## Socioeconomic
                                     GDP + GNI + stability + corruption + internetUsage + phoneUsage))
## model.matrix would drop a country with intercept
## But here we add the intercept column, because we're using shrinkage on country effects but not on intercept.
## X <- cbind(1,X)
## colnames(X)[1] <- "Intercept"
## Some renaming of columns
colnames(X) <- sub("wildTRUE", "wild", colnames(X))
colnames(X) <- sub("primaryTRUE", "primary", colnames(X))
colnames(X) <- sub("countryCode", "country", colnames(X))
## Checks
head(X)

## Standardise continuous covariates (i.e. so mean=0, sd=1)
iContinous <- apply(X, 2, function(X) !(min(X)==0 & max(X)==1)) # Identifies which columns are not factors
X[,iContinous] <- X[,iContinous] %>% scale()
summary(X)

## Check iCountry and
if (length(levels(notifData$countryCode)) != length(colnames(X)[grep("country", colnames(X))])) {
  stop("Check all countries are in X")
} else {
  print(paste(length(levels(notifData$countryCode)), "countries in X"))
}

# Convert confDates to indices for allDates, allDays and holidays
iConfDate <- sapply(notifData$confDate, function(x) which(allDates == x))
if (any(allDates[iConfDate] != notifData$confDate)) {
  stop("Check failled for iConfDate")
} else {
  print("iConfDate passed check")
}

# Convert subDates to indices for allDates, allDays and holidays
iSubDate <- sapply(notifData$subDate, function(x) which(allDates == x))
if (any(allDates[iSubDate] != notifData$subDate)) {
  stop("Check failled for iSubDate")
} else {
  print("iSubDate passed check")
}


##################
##################
## NIMBLE MODEL ##
##################
##################
bugsCode <- nimbleCode ({
  log(rho)   ~ dnorm(mean=0, sd=1000) ## Shape parameter of Weibull. shape=1 for exponential
  log(beta0) ~ dnorm(mean=0, sd=1000) ## Intercept
  for (ii in 1:(nBetaCon + nBetaTV)) {
    beta[ii]     ~ ddexp(location=0, scale=phi[ii]) ## double exponential. scale = force of shrinkage (smaller = stronger)
    log(phi[ii]) ~ dLogGamma(shape=1/2, rate=1/2)   ## shape=1 => dexp. shape -> inf => dnorm. shape -> 0 increases mass at 0
  }
  for (ii in 1:nBetaTV) {
    iTV[ii] <- nBetaCon + ii ## Set of indices for the bate of 9 time-varying covariates
  }
  for (iObs in 1:nObs) {
    ## lambda (hazard-scale) - constant component
    hazardScale[iObs] <- exp(beta0 + inprod(beta[1:nBetaCon], X[iObs, 1:nBetaCon]))
    ## lambda (hazard-scale) - time-varying component
    for (iDay in 1:nDays[iObs]) {
      hazardScaleTV[iObs,iDay] <- exp(
          beta[iTV[1]] * (iDay == 1) +                                            ## Day-zero
          beta[iTV[2]] * (allDays[iConfDate[iObs]+iDay-1] == 1) +                 ## Monday
          beta[iTV[3]] * (allDays[iConfDate[iObs]+iDay-1] == 2) +                 ## Tuesday
          beta[iTV[4]] * (allDays[iConfDate[iObs]+iDay-1] == 3) +                 ## Wednesday
          beta[iTV[5]] * (allDays[iConfDate[iObs]+iDay-1] == 4) +                 ## Thursday
          beta[iTV[6]] * (allDays[iConfDate[iObs]+iDay-1] == 5) +                 ## Friday
          beta[iTV[7]] * (allDays[iConfDate[iObs]+iDay-1] == 6) +                 ## Saturday
          beta[iTV[8]] * (allDays[iConfDate[iObs]+iDay-1] == 7) +                 ## Sunday
          beta[iTV[9]] * (holidays[iConfDate[iObs]+iDay-1, iCountry[iObs]] == 1)  ## Public holiday
      )
    }
    ## Survival
    S[iObs,1] <- sweib(time = 1, shape = rho, lambda = hazardScale[iObs] * hazardScaleTV[iObs,1])
    for (iDay in 2:nDays[iObs]) {
      S[iObs,iDay] <-
        S[iObs,iDay-1] *
        sweib(iDay,   rho, hazardScale[iObs] * hazardScaleTV[iObs,iDay]) /
        sweib(iDay-1, rho, hazardScale[iObs] * hazardScaleTV[iObs,iDay])
    }
    ## p(Y=y) for nDays[iObs]
    probY[iObs,1] <- 1 - S[iObs,1]
    for (iDay in 2:nDays[iObs]) {
      probY[iObs,iDay] <- S[iObs,iDay-1] - S[iObs,iDay]
    }
    ## p(Y>nDays[iObs]) - probability of a right censor beyond nDays[iObs]
    probY[iObs,nDays[iObs]+1] <- max(0, 1 - sum(probY[iObs,1:nDays[iObs]]))
    ## Observed data likelihood
    yPlus1[iObs] ~ dcat(probY[iObs, 1:(nDays[iObs]+1)])
  }
})

## Build nimble model in R
rMod = nimbleModel(bugsCode,
                   const = list(nObs      = nrow(notifData),
                                nBetaCon  = ncol(X),
                                nBetaTV   = 9,
                                nDays     = (notifData$notifDelay + 7),       # Set right censor point for iObs one week after notification date
                                iCountry  = as.integer(notifData$countryCode),
                                iConfDate = iConfDate),
                   inits = list(log_rho   = log(1/w1$scale),
                                log_beta0 = log(w1$coefficients["(Intercept)"]),
                                beta      = rep(0, ncol(X)+9),
                                log_phi   = rep(0, ncol(X)+9)),
                   data  = list(yPlus1    = (notifData$notifDelay + 1),
                                X         = X,
                                allDays   = as.integer(allDays),
                                holidays  = holidays %>% select(!date) %>% as.data.frame()))

## Compile nimble model
cMod = compileNimble(rMod) # Compile model to C++

## Node lists
(stochNodes      <- cMod$getNodeNames(stochOnly=TRUE, includeData=F))
(stochNodesShort <- unique(sub("\\[.*", "", stochNodes)))

## Tinker with initial values
simulate(cMod, "log_phi")
cMod[["log_phi"]]
#simulate(cMod, "beta")
#cMod$beta = cMod$beta * 0

#exp(cMod$log_phi)
#calculate(cMod)
#cMod$X
#sapply(1:nrow(notifData), function(x) (getLogProb(cMod, paste0("yPlus1[",x,"]"))))

## Check stochastic nodes are initialised
cMod$log_rho   # No (NAs)
cMod$log_beta0 # No (NAs)
cMod$beta
cMod$log_phi   # No (NAs)
## Check transformed parameter nodes are initialised
cMod$rho       # Yes
cMod$beta0     # Yes
cMod$phi       # Yes



# Check logProbs - SUPER SLOW!!!
calculate(cMod)
calculate(cMod, "log_rho")
calculate(cMod, "log_phi")
calculate(cMod, "log_beta0")
calculate(cMod, "beta")
calculate(cMod, "probY")
cMod$probY[1,]
cMod$beta


## MCMC with simple Weibull model
conf = configureMCMC(cMod, monitors=c("rho","beta0","beta","phi")) # Configure an MCMC // Monitors: register values for listed parameters -> creates a table for accepted values of parameters
conf$removeSamplers("log_phi[]")
#conf$addSampler(c("log_phi"), type="RW_block", control=list(scale=0.1))
conf$addSampler(c("log_beta0","beta","log_phi", "log_rho"), type="RW_block", control=list(scale=0.0001))
conf$addSampler("log_phi", type="RW_block", control=list(scale=0.0001))
#conf$addSampler(c("log_beta0","beta", "log_rho"), type="RW_block", control=list(scale=0.1))
## for (ii in 1:ncol(X)) {
##   nodeVec_ii = c(paste0("beta[",ii,"]"),paste0("log_phi[",ii,"]"))
##   conf$addSampler(nodeVec_ii, type="RW_block", control=list(scale=0.0001))
## }
## conf$addSampler("rho", type="RW", control=list(scale=0.0001))
conf

## Build & compile MCMC
mcmcR = buildMCMC(conf)
mcmcC = compileNimble(mcmcR)


nIter = c(1001, 1E4, rep(1E5, 11))
nRuns = length(nIter)
for (iRun in 1:nRuns) {
  ## Run the MCMC
  print(system.time(mcmcC$run(niter=nIter[iRun], reset=(iRun<4), resetMV=(iRun<4), thin=1000)))
  # 1E4 iterations in 4.4 minutes on workstation (12 minutes on laptop)
  # 1E5 iterations in 44 minutes
  # 1E6 iterations in 7.4 hours
  #
  # Extract MCMC output, reorder columns & convert to mcmc object for coda
  samps  <- as.matrix(mcmcC$mvSamples)
  iRho   <- which(colnames(samps)=="rho")
  iBeta0 <- which(colnames(samps)=="beta0")
  iOther <- which(colnames(samps)!="beta0" & colnames(samps)!="rho")
  samps  <- as.mcmc(samps[,c(iRho,iBeta0,iOther)])
  ##########################
  ## trajectoriesMCMC.pdf ##
  ##########################
  pdf(file = here::here("figures/trajectories.pdf"))
  plot(samps)
  dev.off()
  ####################################
  ## Write samps and samps2 to file ##
  ####################################
  write.table(as.matrix(samps), file=here::here("MCMC/samps.txt"), row.names = FALSE)
}





codamenu() #diagnostics of the MCMC // 95% Credibility interval for estimated parameters ( 2 / obj / Output analysis)
summary(samps)
buSamps = samps

## Create pdf of MCMC trajectories
pdf(file=here("MCMC/trajectories.pdf"))
plot(samps)
dev.off()




## Hazard ratios ####

HR_Cluster2<-exp(samps[,"ClusterCluster 2"])
hist(HR_Cluster2, main="Hazard ratio for Cluster 2 vs Cluster 1", xlab="Value of beta", breaks=50)
quantile(HR_Cluster2, c(0.025, 0.975))

HR_Cluster3<-exp(samps[,"ClusterCluster 3"])
hist(HR_Cluster3, main="Hazard ratio for Cluster 3 vs Cluster 1", xlab="Value of beta", breaks=50)
quantile(HR_Cluster3, c(0.025, 0.975))

HR_wild<-exp(samps[,"wild_1Wild"])
hist(HR_wild, main="Hazard ratio for wild animals", xlab="Value of beta", breaks=50)
quantile(HR_wild, c(0.025, 0.975))

HR_primary<-exp(samps[,"primarySecondary"])
hist(HR_primary, main="Hazard ratio for secondary outbreaks compared to primary outbreaks", xlab="Value of beta", breaks=50)
quantile(HR_primary, c(0.025, 0.975))

HR_Holidays<-exp(samps[,"holidayYes"])
hist(HR_Holidays, main="Hazard ratio for outbreaks happening on national holiday", xlab="Value of beta", breaks=50)
quantile(HR_Holidays, c(0.025, 0.975))

HR_Accessibility<-exp(samps[,"accessibility"])
hist(HR_Accessibility, main="Hazard ratio for accessibility", xlab="Value of beta", breaks=50)
quantile(HR_Accessibility, c(0.025, 0.975))

HR_Evolution<-exp(samps[,"incidence"])
hist(HR_Evolution, main="Hazard ratio for recent case declarations", xlab="Value of beta", breaks=50)
quantile(HR_Evolution, c(0.025, 0.975))

HR_Vet_Density<-exp(samps[,"vetDensity"])
hist(HR_Vet_Density, main="Hazard ratio for vet density", xlab="Value of beta", breaks=50)
quantile(HR_Vet_Density, c(0.025, 0.975))

HR_Ducks<-exp(samps[,"Ducks_density"])
hist(HR_Ducks, main="Hazard ratio for nearby duck density", xlab="Value of beta", breaks=50)
quantile(HR_Ducks, c(0.025, 0.975))

HR_Chicken<-exp(samps[,"Chicken_density"])
hist(HR_Chicken, main="Hazard ratio for nearby chicken density", xlab="Value of beta", breaks=50)
quantile(HR_Chicken, c(0.025, 0.975))

HR_Admin3=exp(samps[,"adminLevels3"])
hist(HR_Admin3, main="Hazard ratio for 3 levels of administrations vs 2", xlab="Value of beta", breaks=50)
quantile(HR_Admin3, c(0.025, 0.975))

HR_Admin4=exp(samps[,"adminLevels4"])
hist(HR_Admin4, main="Hazard ratio for 4 levels of administrations vs 2", xlab="Value of beta", breaks=50)
quantile(HR_Admin4, c(0.025, 0.975))

HR_Lundi=exp(samps[,"weekDaylundi"])
HR_Mardi=exp(samps[,"weekDaymardi"])
HR_Mercredi=exp(samps[,"weekDaymercredi"])
HR_Jeudi=exp(samps[,"weekDayjeudi"])
HR_Vendredi=exp(samps[,"weekDayvendredi"])
HR_Samedi=exp(samps[,"weekDaysamedi"])

hist(HR_Lundi, main="Hazard ratio for reports on Monday vs Sunday", xlab="Value of beta", breaks=50)
hist(HR_Mardi, main="Hazard ratio for reports on Tuesday vs Sunday", xlab="Value of beta", breaks=50)
hist(HR_Mercredi, main="Hazard ratio for reports on Wednesday vs Sunday", xlab="Value of beta", breaks=50)
hist(HR_Jeudi, main="Hazard ratio for reports on Thursday vs Sunday", xlab="Value of beta", breaks=50)
hist(HR_Vendredi, main="Hazard ratio for reports on Friday vs Sunday", xlab="Value of beta", breaks=50)
hist(HR_Samedi, main="Hazard ratio for reports on Saturday vs Sunday", xlab="Value of beta", breaks=50)

quantile(HR_Lundi, c(0.025, 0.975))
quantile(HR_Mardi, c(0.025, 0.975))
quantile(HR_Mercredi, c(0.025, 0.975))
quantile(HR_Jeudi, c(0.025, 0.975))
quantile(HR_Vendredi, c(0.025, 0.975))
quantile(HR_Samedi, c(0.025, 0.975))


HR_JeudiVendredi=exp(samps[,"weekDayjeudi"])/exp(samps[,"weekDayvendredi"])
hist(HR_JeudiVendredi, main="Hazard ratio for reports on Thursday vs Friday", xlab="Value of beta", breaks=50)
HR_VendrediSamedi=exp(samps[,"weekDayvendredi"])/exp(samps[,"weekDaysamedi"])
hist(HR_VendrediSamedi, main="Hazard ratio for reports on Friday vs Saturday", xlab="Value of beta", breaks=50)
HR_JeudiSamedi=exp(samps[,"weekDayjeudi"])/exp(samps[,"weekDaysamedi"])
hist(HR_JeudiSamedi, main="Hazard ratio for reports on Thursday vs Saturday", xlab="Value of beta", breaks=50)

quantile(HR_JeudiVendredi, c(0.025, 0.975))
quantile(HR_VendrediSamedi, c(0.025, 0.975))
quantile(HR_JeudiSamedi, c(0.025, 0.975))

################## good luck! ########################
samps = as.mcmc(as.matrix(mcmcC$mvSamples)) #extract the MCMC output & convert it to an mcmc object using the coda package.
colnames(samps)[grep(pattern = "beta", colnames(samps))]
colnames(samps)[grep(pattern = "beta", colnames(samps))]
colnames(samps)[grep(pattern = "beta", colnames(samps))]
colnames(samps)[grep(pattern = "beta", colnames(samps))]
#<-
substring(colnames(X)[-1], 12, 100)
#<-
substring(colnames(X), 12, 100)
colnames(samps)[grep(pattern = "beta", colnames(samps))]<-substring(colnames(X), 12, 100)
colnames(samps)[2] <- "beta0"
summary(samps)
predTimes = c(0, km$time) # For survival curve
nTimes = length(predTimes)
surv = array(NA, dim=c(nrow(samps), nTimes)) #Matrix
pSeq = seq(0.01, 0.99, by=0.01) # For Q-Q plot
quant = array(NA, dim=c(nrow(samps), length(pSeq)))
for (ii in 1:nrow(samps)) { ##For each MCMC iteration, quantify surv and quantiles
  if (ii %% (nrow(samps)/10) == 0)
    nimPrint(ii, "/", nrow(samps)) # Print progress
  surv[ii,] = 1-pweibull(predTimes, shape=samps[ii,"rho"], scale=exp(samps[ii,"beta0"]))
  quant[ii,] = qweibull(pSeq, shape=samps[ii,"rho"], scale=exp(samps[ii,"beta0"]))
}
# Calculate quantiles for survival curve and Q-Q plot // synthesis of loop output
survMedianCI95 = apply(surv, 2, quantile, probs = c(0.025, 0.5, 0.975)) ##generates surv curve
quantMedianCI95 = apply(quant, 2, quantile, probs = c(0.025, 0.5, 0.975)) ##generates qqplot
par(mfrow=c(1,1))
plot(km, lwd=3, ylab="Survival", xlab="Time")
polygon(x = c(predTimes, rev(predTimes)),
        y = c(survMedianCI95[1, ], rev(survMedianCI95[3, ])),
        col = "lightblue", # 0.50
        border = NA
)
lines(predTimes, survMedianCI95[2, ], col="blue", lwd=3, lty=2)
legend("topright", col=c("black", "blue", "lightblue"), lwd=3, cex=0.8,
       legend = c("Kaplan-Meier", "Posterior Median", "Bayesian 95% CI"))
plot(km, lwd=3, ylab="Survival", xlab="Time")
polygon(x = c(predTimes, rev(predTimes)),
        y = c(survMedianCI95[1, ], rev(survMedianCI95[3, ])),
        col = rgb(0, 0, 1 ,0.5), # 0.50
        border = NA
)
lines(predTimes, survMedianCI95[2, ], col="blue", lwd=3, lty=2)
summary(samps)
samps
HR_wild<-exp(samps[,"wild_1Wild"])/exp(samps[,"wild_1Domestic"])
HR_wild<-exp(samps[,"wild_1Wild"])/exp(samps[,"wild_1Domestic"])
HR_wild<-exp(samps[,"wild_1Wild"])
hist(HR_wild)
hist(HR_wild, breaks=50)
HR_Accessibility<-exp(samps[,"accessibility"])
hist(HR_Accessibility)
hist(HR_Accessibility, breaks=50)
boxplot(notifData$notifDelay~notifData$wild)
boxplot(sqrt(notifData$notifDelay)~notifData$wild)
HR_primary<-exp(samps[,"primarySecondary"])
hist(HR_primary)
boxplot(notifData$notifDelay~notifData$primary)
boxplot(sqrt(notifData$notifDelay)~notifData$primary)
summary(samps)
HR_Holidays<-exp(samps[,"holidayYes"])
HR_Holidays
hist(HR_Holidays)
HR_Weekdaay<-exp(samps[,"weekDayWednesday"])
HR_Weekdaay<-exp(samps[,"weekDaymercredi"])
hist(HR_Weekdaay)
HR_Weekdaay<-exp(samps[,"weekDaysamedi"])
hist(HR_Weekdaay)
HR_Weekdaay<-exp(samps[,"weekDaydimanche"])
HR_Weekdaay<-exp(samps[,"weekDaylundi"])
hist(HR_Weekdaay)
HR_DucksDensity<-exp(samps[,"Ducks_density"])
HR_DucksDensity
hist(HR_DucksDensity)
HR_Cluster3<-exp(samps[,"ClusterCluster 3"])
hist(HR_Cluster3)
km2 = survfit(Surv(time=notifData$notifDelay, time2=(notifData$notifDelay+1), event=notifData$Censored, type="interval2")~Cluster, data=notifData) ## ???????
plot(km2)
predTimes = c(0, km$time) # For survival curve
nTimes = length(predTimes)
surv = array(NA, dim=c(nrow(samps), nTimes)) #Matrix
pSeq = seq(0.01, 0.99, by=0.01) # For Q-Q plot
quant = array(NA, dim=c(nrow(samps), length(pSeq)))
for (ii in 1:nrow(samps)) { ##For each MCMC iteration, quantify surv and quantiles
  if (ii %% (nrow(samps)/10) == 0)
    nimPrint(ii, "/", nrow(samps)) # Print progress
  surv[ii,] = 1-pweibull(predTimes, shape=samps[ii,"rho"], scale=exp(samps[ii,"beta0"]+samps[ii,"ClusterCluster 3"]))
  surv2[ii,] = 1-pweibull(predTimes, shape=samps[ii, "rho"], scale=exp[samps[ii,"beta0"]])
}
surv2 = surv
pSeq = seq(0.01, 0.99, by=0.01) # For Q-Q plot
quant = array(NA, dim=c(nrow(samps), length(pSeq)))
for (ii in 1:nrow(samps)) { ##For each MCMC iteration, quantify surv and quantiles
  if (ii %% (nrow(samps)/10) == 0)
    nimPrint(ii, "/", nrow(samps)) # Print progress
  surv[ii,] = 1-pweibull(predTimes, shape=samps[ii,"rho"], scale=exp(samps[ii,"beta0"]+samps[ii,"ClusterCluster 3"]))
  surv2[ii,] = 1-pweibull(predTimes, shape=samps[ii, "rho"], scale=exp[samps[ii,"beta0"]])
}
for (ii in 1:nrow(samps)) { ##For each MCMC iteration, quantify surv and quantiles
  if (ii %% (nrow(samps)/10) == 0)
    nimPrint(ii, "/", nrow(samps)) # Print progress
  surv[ii,] = 1-pweibull(predTimes, shape=samps[ii,"rho"], scale=exp(samps[ii,"beta0"]+samps[ii,"ClusterCluster 3"]))
  surv2[ii,] = 1-pweibull(predTimes, shape=samps[ii, "rho"], scale=exp(samps[ii,"beta0"]))
}
surv0 = surv
pSeq = seq(0.01, 0.99, by=0.01) # For Q-Q plot
quant = array(NA, dim=c(nrow(samps), length(pSeq)))
for (ii in 1:nrow(samps)) { ##For each MCMC iteration, quantify surv and quantiles
  if (ii %% (nrow(samps)/10) == 0)
    nimPrint(ii, "/", nrow(samps)) # Print progress
  surv[ii,] = 1-pweibull(predTimes, shape=samps[ii,"rho"], scale=exp(samps[ii,"beta0"]+samps[ii,"ClusterCluster 3"]))
  surv0[ii,] = 1-pweibull(predTimes, shape=samps[ii, "rho"], scale=exp(samps[ii,"beta0"]))
}
plot(surv0)
par(mfrow=c(1,1))
plot(km, lwd=3, ylab="Survival", xlab="Time")
polygon(x = c(predTimes, rev(predTimes)),
        y = c(survMedianCI95[1, ], rev(survMedianCI95[3, ])),
        col = rgb(0, 0, 1 ,0.5), # 0.50
        border = NA
)
# Calculate quantiles for survival curve and Q-Q plot // synthesis of loop output
survMedianCI95 = apply(surv, 2, quantile, probs = c(0.025, 0.5, 0.975)) ##generates surv curve
# Calculate quantiles for survival curve and Q-Q plot // synthesis of loop output
survMedianCI95 = apply(surv0, 2, quantile, probs = c(0.025, 0.5, 0.975)) ##generates surv curve
survMedianCI95 = apply(surv, 2, quantile, probs = c(0.025, 0.5, 0.975)) ##generates surv curve
# Calculate quantiles for survival curve and Q-Q plot // synthesis of loop output
survMedianCI95_0 = apply(surv0, 2, quantile, probs = c(0.025, 0.5, 0.975)) ##generates surv curve
survMedianCI95 = apply(surv, 2, quantile, probs = c(0.025, 0.5, 0.975)) ##generates surv curve
plot(km, lwd=3, ylab="Survival", xlab="Time")
polygon(x = c(predTimes, rev(predTimes)),
        y = c(survMedianCI95[1, ], rev(survMedianCI95[3, ])),
        col = rgb(1, 0, 0 ,0.2), # 0.50
        border = NA
)
polygon(x = c(predTimes, rev(predTimes)),
        y = c(survMedianCI95_0[1, ], rev(survMedianCI95_0[3, ])),
        col = rgb(0, 0, 1 ,0.2), # 0.50
        border = NA
)
summary(samps)
samps[,"primaryPrimary"]
samps[,"primarySecondary"]
HR_Cluster3<-exp(samps[,"ClusterCluster 3"])
hist(HR_Cluster3) ##Illogical
summary(samps[,"ClusterCluster 3"])
HR_Cluster3<-exp(samps[,"ClusterCluster 3"])
HR_Cluster3b<- exp(samps[,"beta0"]+samps[,"ClusterCluster 3"])/exp(samps[,"beta0"])
hist(HR_Cluster3)  ##Illogical
hist(HR_Cluster3b) ##Illogical
hist(HR_Cluster3b) ##Illogical
hist(HR_Cluster3)  ##Illogical
hist(HR_Cluster3b) ##Illogical
HR_Cluster3<-exp(-samps[,"ClusterCluster 3"])
hist(HR_Cluster3)  ##Illogical
summary(X)
X*
  X
HR_primary_corrected<-exp(-samps[,"primarySecondary"]*samps[,"rho"])
hist(HR_primary_corrected)
hist(HR_primary_corrected, breaks=50)
hist(HR_primary) #Illogical
hist(HR_primary, breaks=50) #Illogical
hist(HR_primary_corrected, breaks=50)
getwd()


?history
savehistory(file = "~/timelines*/.Rhistory")
savehistory(file = ".Rhistory")
