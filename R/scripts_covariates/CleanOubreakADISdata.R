
library(tidyverse)
library(readxl)     # to read xls file (ADIS file)
library(lubridate)  # to play with dates
library(here)       # here::here returns path to project directory


## open outbreak file
data_ADIS<-read_excel(here("data/outbreaks/adis-outbreaks-IAHP_2016_2021.xls")) ## File downloaded from ADIS

## Rename variables
data_ADIS <- data_ADIS %>%
  rename(confDate = `Confirmation date`,
         subDate  = `Submission date`,
         country  = `Country/Territory`,
         primary  = Type) %>%
  mutate (confDate = as.Date(confDate),
          subDate  = as.Date(subDate))

## select the study period (to be discussed)
data_ADIS_study <- subset (data_ADIS, subDate>="2016-01-01" & subDate<= "2019-12-31")

## Create delay variable
data_ADIS_study <- data_ADIS_study %>%
  mutate (notifDelay = subDate - confDate)

## Rename UK and Slovakia (to match with covariates data)
data_ADIS_study[data_ADIS_study$country=="United Kingdom ( -2020)","country" ] <- "United Kingdom"
data_ADIS_study[data_ADIS_study$country=="Slovakia","country"] <- "Slovak Republic"

## Create wild variable (TRUE for wild, FALSE for domestic)
data_ADIS_study <- data_ADIS_study %>%
  mutate (wild = ifelse (`Disease name`== "Highly pathogenic avian influenza (poultry)", FALSE, TRUE))

## create ID number
data_ADIS_study <- data_ADIS_study %>%
  mutate (ID= seq (1: nrow(data_ADIS_study)))

## Keep variables of interest
data_ADIS_study <- data_ADIS_study %>%
  dplyr::select (ID,  country,  subDate, confDate, notifDelay, wild, primary,  Latitude, Longitude )


write.csv2(data_ADIS_study, here("data/outbreaks/clean_data_ADIS_hpai_europe.csv"), row.names=F)
