# CleanOubreakADISdata.R
Takes ADIS excel file, filters period and columns, renames columns.
BUT, ADIS file structures change very regularly (every 2-3 months), so only works for files downloaded during a specific period.

# Covariates.R
Opens all covariates files, and joins with outbreak data, via country and period (various temporal resolutions).
Uses geographical coordinates to extract various density estimates.
